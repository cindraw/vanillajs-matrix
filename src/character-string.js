class CharacterString {
	constructor({
		id: id,
	}) {
		this.id = id
		// get coordinates from init object, column is id number
		this.xcoordinates = init.arrays.coordinates[this.id]
		// get maxlength from settings obj and save privately or else calling it from methods glitches app
		this.maxlength = settings.maxLength

		this.characters
		this.gradient
		this.fade
		this.engine
		this.speed
		this.glitchInterval

		// init and glitch
		this.initAll()
		this.glitch()

	}

	initAll() {
		this.engine = 0
		this.speed = settings.randomSpeed
		// get characters frpm init object
		this.characters = init.arrays.characters[ranf(this.maxlength)].slice()
		// get random fade
		let ran = ranf(this.maxlength)
		this.gradient = init.arrays.gradient[ran].slice()
		this.fade = init.arrays.fade[ran].slice()
		// set start position by adding height amount of zeros plus a random amount
		let startposition = new Array(this.characters.length + ranf(100)).join(0).split('')
		// add start positions to gradient and fade arrays
		this.gradient = [...startposition, ...this.gradient]
		this.fade = [...startposition, ...this.fade]
	}

	/* called from main draw() */
	updateArrays() {
		this.engine++
		if (this.engine % this.speed === 0) {
			let len = this.fade.length > 0
			if (len) {
				this.fade.shift()
				this.gradient.shift()
			} else {
				this.initAll()
			}
		}
	}

	/* glitch changes characters from the characters string at random  */
	glitch() {
		clearInterval(this.glitchInterval)
		this.glitchInterval = setInterval(() => {
			let
				len = this.characters.length,
				random = ranf(len),
				charac = ranf(10) > 5 ? character.newGlitch : character.newChar,
				newArray = this.characters.split('')
			newArray[random] = charac
			this.characters = newArray.join('')
		}, 12)
	}

}

// return trunced random number
function ranf(num) {
	return Math.floor(Math.random() * num)
}