/* testing mode for app */
const test = false

/*  set global settings mouseposition in engine object */
q.addEventListener('mousemove', () => {
	engine.mousePosition(event)
	menudiv.show()
})

/* show hide menu on mouse move */
const
	menudiv = {
		menu: document.getElementById('menu'),
		canvas: document.getElementById('q'),
		timeout: undefined,
		show() {
			this.menu.style.display = 'block'
			this.canvas.style.cursor = 'default'
			clearTimeout(this.timeout)
			this.timeout = setTimeout(() => this.hide(), 1000)
		},
		hide() {
			this.menu.style.display = 'none'
			this.canvas.style.cursor = 'none'
		}
	}

/* set canvas (width, height) */
let
	win = {
		s: window.screen,
		get width() {
			return q.width = this.s.width
		},
		get height() {
			return q.height = this.s.height
		}
	}

/* characters */
let
	character = {
		test: test,
		katakana: `アァタナハハマヤャラワガザダバパイィキシチニヒミリヰギジヂビピプブヅズグルュユムフヌツスクゥウエェケセテネヘメレヱゲゼデベペポボドゾゴヲロョヨモホノトソコォオヴッン・ーヽヾ、。'`, // https://www.lexilogos.com/keyboard/katakana.htm
		westernup: `ABCDEFGHIJKLMNOPQRSTUVWXYZ`,
		westernlo: `abcdefghijklmnopqrstuvwxyz`,
		numbers: `0123456789`,
		symbols: `!@#$%^›&*(){}[]?><,./;':"-=_+`,
		get characters() {
			return this.test ? this.westernlo : this.katakana
		},
		get glitch() {
			return this.numbers + this.symbols + this.westernlo + this.westernup
		},

		// these functions are called from within the CharacterString class
		get newChar() {
			return this.characters.charAt(Math.random() * this.characters.length)
		},
		get newGlitch() {
			return this.glitch.charAt(Math.random() * this.glitch.length)
		}
	}

/* animation engine */
let
	engine = {
		interval: undefined,
		fps: 33,
		run() {
			clearInterval(this.interval)
			this.interval = setInterval(draw, this.fps)
		},
		stop() {
			clearInterval(this.interval)
		},
		mouseX: undefined,
		mouseY: undefined,
		isMouseMoving: false,
		mousePosition(event) {
			this.isMouseMoving = true
			this.mouseX = event.clientX
			this.mouseY = event.clientY
		},
		/*  used in draw funciton to detect collision with letter */
		collisionDetection(x, y) {
			return (x > this.mouseX - object.size && x < this.mouseX && y > this.mouseY && y < this.mouseY + object.size)
		}
	}

/* app settings */
let
	settings = {
		size: 14,
		spacing: 0.6,
		align: 'center',
		minSpeed: 1,
		maxSpeed: 4,
		colorintensity: 255,
		minFade: 10,
		get randomSpeed() {
			return Math.trunc(Math.random() * this.maxSpeed) + this.minSpeed
		},
		get maxWidth() {
			return Math.ceil(win.width / this.size)
		},
		get maxLength() {
			return Math.ceil(win.height / this.size) + 10
		},
		get font() {
			return (this.size * this.spacing) + 'pt Georgia'
		},
	}

/* init settings
declare all the arrays before running the engine so that canvas draws witout having to do heavy calculations that results in glitches
*/
const
	init = {
		arrays: {
			coordinates: [],
			characters: [],
			fade: [], // alpha
			gradient: [] // r,g,b
		},
		run() {
			// create an array of length = maxHeight
			let len = settings.maxLength
			let array = new Array(len).join(0).split('')
			// create as many arrays(columns) to fill the width = maxWidth 
			for (let index = 0; index < settings.maxWidth; index++) {
				this.coordinates(index, array)
				this.characters(index, array)
				this.fades(index, this.plot)
			}
			// return true when done
			return true
		},
		// make character strands of random characters
		characters(id, array) {
			this.arrays.characters[id] = array.map(i => i = character.newChar).join('')
		},
		// plot all coordinates to cover the screen
		coordinates(id, array) {
			let
				xpos = id,
				size = settings.size

			this.arrays.coordinates[id] = array.map((i, index) =>
				i = {
					x: xpos * size + size / 2,
					y: index * size
				}
			).reverse()
		},
		/* calculate color and opacity fades
		this is done gradually with id that can be called at random from within the charactesrtring class */
		fades(id, a) {
			let
				grad = a(id + settings.minFade).map(i => i = Math.trunc(settings.colorintensity * i)),
				fade = grad.map((i, index) => i = parseFloat((index / grad.length).toFixed(2)))

			this.arrays.gradient[id] = [255, ...grad.reverse()]
			this.arrays.fade[id] = [1, ...fade.reverse()]
		},
		// plot fade 
		plot(amount) {
			let counter = 0,
				plots = [],
				increase = Math.PI / amount

			for (i = 0; i <= 1; i += 1 / amount * 1.3) {
				counter += increase
				y = counter * counter / 10
				plots.push(y)
			}
			return plots
		},

	}

//fill screen with CharacterStrings and send id (fill row with columns of characters initialized in init object)
let app = {
	strings: [],
	run() {
		for (let k = 0; k < settings.maxWidth; k++) {
			this.strings[k] = new CharacterString({
				id: k,
			})
		}
		return true
	}
}

/* canvas functions  */
let ctx = q.getContext('2d'),
	windowwidth = win.width,
	windowheight = win.height

const draw = () => {
	// background black
	ctx.fillStyle = 'rgba(0, 0, 0, 1)'
	ctx.fillRect(0, 0, windowwidth, windowheight)

	let limit = app.strings.length

	for (let j = 0; j < limit; j++) {
		let s = app.strings[j]
		let posi = s.xcoordinates.slice()
		let lett = s.characters.slice()
		let fade = s.fade.slice()
		let grad = s.gradient.slice()
		s.updateArrays()

		for (k in posi) {
			// [character, position x, position y]
			let [c, x, y] = [lett[k], posi[k].x, posi[k].y]

			// gradient and opacity mapping. if indefined set to 0
			let g = grad[k] ? grad[k] : 0,
				f = fade[k] ? fade[k] : 0

			//sparkle
			f = f > 0 && f < Math.random() * 0.7 ? f += Math.random() * f / 2 : f

			// set color properties, fade from whitish (g) to green (255)
			let [red, green, blue, alpha] = [g, 255, g, f]

			// canvas text settings: fill, font and align
			ctx.fillStyle = `rgba(${red}, ${green}, ${blue}, ${alpha})`
			ctx.font = settings.font
			ctx.textAlign = settings.align

			// position and character
			q.getContext('2d').fillText(c, x, y)
		}
	}

}


pause.onclick = () => engine.stop()
play.onclick = () => engine.run()

//app.start
window.onload = () => {
	menudiv.hide()
	if (init.run()) app.run(), engine.run()
}